Summary: Python Monitoring Plugins Library
Name: python3-nap
Version: 0.1.22
Release: 1%{?dist}
Source: %{name}-%{version}.tar.gz
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Url: https://gitlab.cern.ch/etf/nap
BuildRequires: python3-setuptools

%description
Library to help write monitoring plugins in python

%prep
%autosetup -n %{name}-%{version}

%build
%{__python3} setup.py build

%install
%py3_install

%clean
rm -rf $RPM_BUILD_ROOT

%files -n %{name}
%defattr(-,root,root)
%doc README.md
%{python3_sitelib}/*
